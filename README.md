# Wordpress - Docker - For themes

A Wordpress theme development environment.

## Requirements

- [Docker Compose]

[Docker Compose]: https://docs.docker.com/compose/install/

## Setup

```shell
$ git clone https://gitlab.com/merlinf/wordpress-docker-for-themes.git
$ cd wordpress-docker-for-themes
$ docker-compose up
```

Now visit [localhost:8000] and do the quick wordpress setup.

[localhost:8000]: http://localhost:8000

## Usage

Simply create a subdirectory inside the `themes` directory and develop your theme there.  
Note that the themes directory is created after finishing the setup.